import random

from datetime import datetime, timedelta

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from admin import User, Role, Problem, RequestSeverity, \
    RequestStatus, Request, Toilet


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.sqlite'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)

def add_to_db(obj):
    db.session.add(obj)
    db.session.commit()

    return obj

def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return (start + timedelta(seconds=random_second))

roles = []
for role in ['admin', 'manager', 'contractor']:
    roles.append(add_to_db(Role(name=role)))

users = []
for u in [('admin', 'dream'), \
    ('toufeeq', 'dream'), \
    ('briehan', 'dream'), \
    ('archie', 'dream'), \
    ('motse', 'dream'), \
    ('hiran', 'dream')]:
    user = User()
    user.login = u[0]
    user.password = u[1]
    user.role = random.choice(roles)
    users.append(add_to_db(user))

problems = []
for code, name in enumerate(['Broken Pan', 'Broken Flush', 'Broken Pipe']):
    problems.append(add_to_db(Problem(name=name, code=code)))

severities= []
for i in range(1, 6):
    severities.append(add_to_db(RequestSeverity(name=str(i))))

statuses = []
for name in ['Requested', 'In Progress', 'Requires Verification', 'Completed']:
    statuses.append(add_to_db(RequestStatus(name=name)))

toilets = []
for i in range(10000):
    type = random.choice(['Pit', 'Dry', 'Chemical', 'Portable', 'Urinal'])
    location = random.choice(['Khayelitsha', 'Langa', 'Dunoon', 'Nyanga', 'Guguletu'])
    code = 'WATR-%s%s' % (random.choice(['T', 'S']), str(i).zfill(4))

    toilets.append(Toilet(type=type, location=location, code=code))

d1 = datetime.strptime('1/1/2008 1:30 PM', '%m/%d/%Y %I:%M %p')
d2 = datetime.strptime('1/1/2009 4:50 AM', '%m/%d/%Y %I:%M %p')

requests = []
for i in range(300):
    sid = str(random.randint(99999999, 999999999)).zfill(20)
    problem = random.choice(problems)
    datetime = random_date(d1, d2)
    severity = random.choice(severities)
    status = random.choice(statuses)
    toilet = random.choice(toilets)
    # *toilet*damage_code*severity#
    sequence = '*%s*%s*%s#' % (
        str(toilet.code),
        str(random.choice(problems).code),
        str(severity.name)
    )
    try:
        contractor = random.choice([u for u in users if u.role.name == 'contractor'])
    except:
        contractor = None
    phone_number = '+27%d' % (random.randint(1000000000, 9999999999))

    if status.name == 'Completed' and random.random() > 0.5:
        resolved_datetime = random_date(d1, d2)

    requests.append(add_to_db(Request(sid, datetime=datetime, sequence=sequence, severity=severity,
        status=status, toilet=toilet, contractor=contractor, phone_number=phone_number, problem=problem)))

