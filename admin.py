import datetime
import logging

from flask import Flask, Response, url_for, redirect, render_template, request
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext import admin, login, wtf
from flask.ext.admin.contrib import sqlamodel
from twilio.rest import TwilioRestClient


logger = logging.getLogger(__name__)

#User credentials
account = "AC171312834cb04509017546b8d20cdc0f"
token = "e284426046e1bbf83311db458e2e9055"




# Create application
app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create in-memory database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.sqlite'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    def __init__(self, name):
        self.name = name

    def __unicode__(self):
        return self.name


# Create user model. For simplicity, it will store passwords in plain text.
# Obviously that's not right thing to do in real world application.
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120))
    password = db.Column(db.String(64))

    role_id = db.Column(db.Integer(), db.ForeignKey('role.id'))
    role = db.relationship('Role',
        backref=db.backref('users', lazy='dynamic'))

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.login


class Problem(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))
    code = db.Column(db.String(2))

    def __init__(self, name, code):
        self.name = name
        self.code = code

    def __unicode__(self):
        return '%s: %s' % (self.code, self.name)


# Severity between 1 and 5
class RequestSeverity(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))

    def __init__(self, name):
        self.name = name

    def __unicode__(self):
        return self.name

# requested, in progress, requires verification, completed
class RequestStatus(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    def __init__(self, name):
        self.name = name

    def __unicode__(self):
        return self.name


class Request(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime)
    sequence = db.Column(db.Integer)
    #adding unique sid  ID 
    sid = db.Column(db.String(80), unique=True)

    severity_id = db.Column(db.Integer, db.ForeignKey('request_severity.id'))
    severity = db.relationship('RequestSeverity',
        backref=db.backref('requests', lazy='dynamic'))

    status_id = db.Column(db.Integer, db.ForeignKey('request_status.id'))
    status = db.relationship('RequestStatus',
        backref=db.backref('requests', lazy='dynamic'))

    toilet_id = db.Column(db.Integer, db.ForeignKey('toilet.id'))
    toilet = db.relationship('Toilet',
        backref=db.backref('requests', lazy='dynamic'))

    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
    problem = db.relationship('Problem',
        backref=db.backref('requests', lazy='dynamic'))


    contractor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    contractor = db.relationship('User',
        backref=db.backref('requests', lazy='dynamic'))

    phone_number = db.Column(db.String(20))
    resolved_datetime = db.Column(db.DateTime)

    def __init__(self, sid, sequence, phone_number, datetime, severity, status, toilet, problem, contractor):
        self.sid = sid
        self.datetime = datetime
        self.sequence = sequence
        self.severity = severity
        self.status = status
        self.toilet = toilet
        self.problem = problem
        self.contractor = contractor
        self.phone_number = phone_number

    def __unicode__(self):
        return '%s - %s' % (str(self.datetime), self.phone_number)


request_problems = db.Table('request_problems',
    db.Column('request_id', db.Integer, db.ForeignKey('request.id')),
    db.Column('problem_id', db.Integer, db.ForeignKey('problem.id'))
)


class Toilet(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    type = db.Column(db.String(50))
    location = db.Column(db.String(50))
    code = db.Column(db.String(10))

    #added some optional gmap value  
    gmap_url = db.Column(db.String(20))

    def __init__(self, type, location, code, gmap_url=None):
        self.type = type
        self.location = location
        self.code = code
        self.gmap_url = gmap_url

    def __unicode__(self):
        return self.code


# Define login and registration forms (for flask-login)
class LoginForm(wtf.Form):
    login = wtf.TextField(validators=[wtf.required()])
    password = wtf.PasswordField(validators=[wtf.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise wtf.ValidationError('Invalid user')

        if user.password != self.password.data:
            raise wtf.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(User).filter_by(login=self.login.data).first()


class RegistrationForm(wtf.Form):
    login = wtf.TextField(validators=[wtf.required()])
    email = wtf.TextField()
    password = wtf.PasswordField(validators=[wtf.required()])

    def validate_login(self, field):
        if db.session.query(User).filter_by(login=self.login.data).count() > 0:
            raise wtf.ValidationError('Duplicate username')


# Initialize flask-login
def init_login():
    login_manager = login.LoginManager()
    login_manager.setup_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)


# Create customized model view class
class MyModelView(sqlamodel.ModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated()


# Create customized index view class
class MyAdminIndexView(admin.AdminIndexView):
    def is_accessible(self):
        return login.current_user.is_authenticated()


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/public')
def public():
    """A list of open requests."""
    requests = []
    for r in Request.query.order_by(Request.severity).order_by(Request.datetime.desc()).all():
        if r.status.name != 'Completed':
            requests.append(r)

    return render_template('public.html', requests=requests)


@app.route('/login/', methods=('GET', 'POST'))
def login_view():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = form.get_user()
        login.login_user(user)
        return redirect(url_for('index'))

    return render_template('form.html', form=form)


@app.route('/register/', methods=('GET', 'POST'))
def register_view():
    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        user = User()

        form.populate_obj(user)

        db.session.add(user)
        db.session.commit()

        login.login_user(user)
        return redirect(url_for('index'))

    return render_template('form.html', form=form)


#show all the toilets
@app.route('/toilet/show')
def show_toilets():
    toilets = Toilet.query.all()
    if toilets == []:
       return 'none to return'
    else :
        return render_template('show_entries.html', toilets=toilets)


@app.route('/toilet/submit')
def submit_toilet():
    #add code to check login etc

    #flash('New Entry Successfully posted')
    return render_template('add_toilet.html')
    #return res_str_


@app.route('/contractor')
def all_requests():
    reqs = Request.query.order_by(Request.datetime.desc()).all()
    if reqs == []:
        return 'Request Table is empty'
    else :
        return render_template('show_entries.html', requests=reqs)


#Add a new toilet
@app.route('/contractor/sendsms', methods=['POST'])
def send_sms():
    if request.form['check'] != None:
        client = TwilioRestClient(account, token)
        phone_number = request.form['phone_number']
        sid = request.form['service_id']

        status = RequestStatus.query.filter_by(name='Completed').first()
        req = Request.query.get(int(request.form['request_id']))
        req.status = status
        db.session.commit()
         
        tmp_str = "Service request {0} has been seen to,thank you ".format(
                        sid,
                       )
        user_str = "{0} has been sent a notification ,thank you".format(phone_number)
        
        message = client.sms.messages.create(to=phone_number, from_="+14692034980",
                                     body=tmp_str)
    else:
        tmp_str  = "none"
    #flash('New Entry Successfully posted')
    return user_str
    #return res_str_

    


#Add a new toilet
@app.route('/toilet/add', methods=['POST'])
def add_toilet():
    #add code to check login etc
    #res_str_ = "{0}  {1} {2} {3}\n".format(
    #                                     request.form['type'], 
    #                                     request.form['location'],
    #                                     request.form['code'],
    #                                     request.form['gmap_url']
    #                                     )
    tmp_toilet = Toilet(
                        request.form['type'],
                        request.form['location'],
                        request.form['code'],
                        request.form['gmap_url']
                       )
    db.session.add(tmp_toilet)
    db.session.commit()
    #flash('New Entry Successfully posted')
    return redirect(url_for('show_toilets'))
    #return res_str_

    

@app.route('/secret/json', methods=['POST'])
def json_update():
    #k = dir(request.form)
    k = request.form['type']
    return  str(k)

@app.route('/sms/update')
def update():
    client = TwilioRestClient(account, token)
    for sms in client.sms.messages.list() :
        if sms.body[0] =='*' and sms.from_ !='+14692034980':
            sms2list = sms.body.split('*')
            try:
                serv_req = Request(sms.sid, sms2list[1], sms.from_, datetime.datetime.now(),
                    RequestSeverity.query.get(int(sms2list[3].strip('#'))), 
                    RequestStatus.query.get(1), 
                    Toilet.query.get(1),
                    Problem.query.get(int(sms2list[2])),
                    User.query.get(1))
                db.session.add(serv_req)
                db.session.commit()
                k= "{0} added".format(sms.body)

            except:
                k= "{0} already there added".format(sms.body)

    return redirect(url_for('all_requests'))


@app.route('/logout/')
def logout_view():
    login.logout_user()
    return redirect(url_for('index'))

if __name__ == '__main__':
    # Initialize flask-login
    init_login()

    # Create admin
    admin = admin.Admin(app, 'Auth', index_view=MyAdminIndexView())

    # Add view
    admin.add_view(MyModelView(User, db.session))
    admin.add_view(MyModelView(Request, db.session))
    admin.add_view(MyModelView(Role, db.session))
    admin.add_view(MyModelView(Problem, db.session))
    admin.add_view(MyModelView(RequestSeverity, db.session))
    admin.add_view(MyModelView(RequestStatus, db.session))
    admin.add_view(MyModelView(Toilet, db.session))

    # Create DB
    db.create_all()

    # Start app
    app.debug = True
    app.run(host='0.0.0.0')



